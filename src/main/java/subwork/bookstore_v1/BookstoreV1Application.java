package subwork.bookstore_v1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookstoreV1Application {

	public static void main(String[] args) {
		SpringApplication.run(BookstoreV1Application.class, args);
	}

}
